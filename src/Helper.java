import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class Helper {

	public final static int CONST_PREFIX = 3452;

	public static String hash(String data) {
		// Hash function goes here...
		String myHash = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update((CONST_PREFIX + data).getBytes());
			byte[] digest = md.digest();
			myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return myHash;
	}
	
}

import java.util.Set;

public class NonSenseBlockChain extends BlockChain {
	
	private NonSenseBlockChain() {

	}

	private DataStore ds = new DataStore();
	
	private static NonSenseBlockChain instance;

	public static NonSenseBlockChain getInstance() {
		if (instance == null) {
			instance = new NonSenseBlockChain();
		}
		return instance;
	}

	public boolean submitBlock(Person person, Block block) {
		if(instance.validateAndAddBlock(block)) {
			ds.assignPersonACoin(person);
			System.out.println("A Nonsense coin has been awarded to "+ person);
			return true;
		}
		System.out.println("The block doesn't seems to be valid");
		return false;
	}
	
	public void displayData() {
		Set<Person> set = ds.getPersonCoins().keySet();
		for(Person p : set) {
			System.out.println(p + " has "+ ds.getPersonCoins().get(p) + " nonsense coins");
		}
	}

}


public class Main {
	
	public static void main(String[] args) {
		NonSenseBlockChain bc = NonSenseBlockChain.getInstance();
		bc.addToChain(new Block("Hello", "DA69DDF4006B520C1E7326ADC8626CB0"));
		bc.addToChain(new Block("Brother", "077B712AE75922CDFBC74DC74AAA5B3F"));
		bc.addToChain(new Block("And", "A90300C7E620EC5BBFEF0FBF642D8208"));
		bc.addToChain(new Block("Sister", "5BEBB134EC2D54F73761F9C43909CA8A"));
		bc.display();
		System.out.println ("Is blockchain valid? "+ bc.validateTransactions());
		
		System.out.println("==================================================================");
		System.out.println("==================================================================");
		System.out.println("==================================================================");
		
		Person aman = new Person(1, "Aman", "Verma");
		Person bharat = new Person(2, "Bharat", "Verma");
		Person yogi = new Person(3, "Yogi", "Verma");
		
		bc.submitBlock(aman, new Block("Another", "E056C52F91348D91F25A67DD95DF48B4"));
		bc.submitBlock(aman, new Block("Another 2", "365C20F1DFB128B3EA905DB290555C39"));
		
		bc.submitBlock(bharat, new Block("Random", "2D790D3A1A61231378FC8492DF2B2D83"));
		bc.submitBlock(yogi, new Block("Thoughts", "6C2266BFE9ED95887D3CC83A11A9EF04"));
		
		bc.displayData();
		
	}
}

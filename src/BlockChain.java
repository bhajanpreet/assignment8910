import java.util.ArrayList;
import java.util.List;

public class BlockChain {

	public BlockChain() {
		Block genesis = new Block("Root", null);
		chain.add(genesis);
	}

	private List<Block> chain = new ArrayList<Block>();

	public Block lastBlock() {
		return chain.get(chain.size() - 1);
	}
	
	public boolean validateAndAddBlock(Block block) {
		Block lastBlock = chain.get(chain.size() - 1);
		if(block.getHash().equals(Helper.hash(lastBlock.getHash() + block.getData()))){
			return chain.add(block);	
		}
		return false;
	}

	public boolean addToChain(Block block) {
		return chain.add(block);
	}
	
	
	public void display() {
		for (Block block : chain) {
			System.out.println("****************************************");

			System.out.println(block.getData());
			System.out.println(block.getHash());

			System.out.println("****************************************");
		}
	}

	public boolean validateTransactions() {
		for (int i = 1; i < chain.size(); i++) {
			String lastHash = chain.get(i - 1).getHash();
			Block b = chain.get(i);
			String hash = Helper.hash(lastHash + b.getData());
			if (!hash.equals(b.getHash())) {
				return false;
			}
		}
		return true;
	}
}

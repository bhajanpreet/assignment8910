public class Block {

	public Block(String data, String previousHash) {
		this.data = data;
		String dataToBeHashed = previousHash == null ? data : previousHash + data;
		this.hash = Helper.hash(dataToBeHashed);
		this.previousHash = previousHash;
	}

	private String data;

	private String hash;

	private String previousHash;

	public String getPreviousHash() {
		return previousHash;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
		this.hash = Helper.hash(data);
	}

	public String getHash() {
		return hash;
	}

}

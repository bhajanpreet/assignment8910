import java.util.HashMap;

public class DataStore {
	private HashMap<Person, Integer> personCoins =  new HashMap<Person, Integer>();

	public HashMap<Person, Integer> getPersonCoins() {
		return personCoins;
	}

	public void assignPersonACoin(Person person) {
		if(!personCoins.containsKey(person)) {
			personCoins.put(person, 1);
		} else {
			personCoins.put(person, personCoins.get(person) + 1);
		}
	}	
}
